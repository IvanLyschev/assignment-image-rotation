//
// Created by ivan on 3/7/22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_H
#define ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_H

#include "../image/image.h"
struct image_optional rotate(struct image const source);

#endif //ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_H
