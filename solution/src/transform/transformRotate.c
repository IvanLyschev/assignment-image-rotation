//
// Created by ivan on 3/12/22.
//

#include "transformRotate.h"


struct image_optional rotate(struct image const source){
    const uint64_t width = source.width;
    const uint64_t height = source.height;
//    struct pixel *pixels = source.pixels;
    // create new image
    struct image_optional optional = image_create(width, height);
    if (!optional.is_valid) {
        return image_none;
    }
    struct pixel *copy_pixels = optional.image.pixels;
    for (uint64_t y = 0; y < height; ++y) {
        for (uint64_t x = 0; x < width; ++x) {
            *getPixel(optional.image, x * height + (height - 1 - y)) = *getPixel(source, y *width + x);
//            *(copy_pixels + x * height + (height - 1 - y))
//                    = *(pixels + y * width + x);

        }
    }

    return toOptional((struct image) {
            .width = optional.image.height,
            .height = optional.image.width,
            .pixels = copy_pixels
    });
}

