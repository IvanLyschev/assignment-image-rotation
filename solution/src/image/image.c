//
// Created by ivan on 3/7/22.
//
#include <malloc.h>

#include "image.h"


struct image_optional toOptional(struct image img){
    return (struct image_optional){.is_valid = true,.image = img};
}

struct image_optional image_create(uint64_t width, uint64_t height){
    void* pixels = malloc(sizeof(struct pixel) * width * height);

    if (pixels == NULL) {
        return image_none;
    } else {
        return toOptional((struct image) {.width = width, .height = height, .pixels = pixels});
    }
}
void image_free(struct image *img){
    free(img->pixels);
}

struct pixel * getPixel(struct image img, uint64_t off){
    struct pixel *pixels = img.pixels;
    return pixels + off;
}
