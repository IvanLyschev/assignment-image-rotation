//
// Created by ivan on 3/7/22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <malloc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>


struct image {
    uint64_t width, height;
    struct pixel *pixels;
};

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

struct image_optional {
    bool is_valid;
    struct image image;
};
static const struct image_optional image_none = {0};
struct image_optional toOptional(struct image img);
struct image_optional image_create(uint64_t width, uint64_t height);
void image_free(struct image *img);
struct pixel * getPixel(struct image img, uint64_t off);


#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
