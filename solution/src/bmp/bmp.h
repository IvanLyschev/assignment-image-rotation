//
// Created by ivan on 3/7/22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include "../image/image.h"
#include <inttypes.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include  <stdint.h>

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_STREAM_NULL,
    READ_NULL_TARGET,
    READ_MALLOC_ERROR
};

enum read_status from_bmp(FILE *in, struct image *img);

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_SOURCE_NULL
};

enum write_status to_bmp(FILE *out, struct image const *img);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
