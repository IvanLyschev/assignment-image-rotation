//
// Created by ivan on 3/12/22.
//


#include "bmp.h"

#define SIGNATURE 19778
#define RESERVED 0
#define HEADER_SIZE 40
#define PLANES 1
#define COMPRESSION 0
#define PIXEL_PER_M 2834
#define COLORS_USED 0
#define COLORS_IMPORTANT 0
#define DOUBLE_WORD 4
#define BIT_COUNT 24

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static enum read_status read_bmp_header(FILE *in, struct bmp_header *header_ptr);
static void read_image_pixels_heap(FILE *in, struct image *img);
static enum write_status write_image_pixels(FILE* file, const struct image* img);
static enum write_status create_bmp_header(struct image const *img, struct bmp_header *header);
static size_t calculate_padding(size_t width);

enum read_status from_bmp(FILE *in, struct image *img) {
    if (!in) { return READ_STREAM_NULL; }
    if (!img) { return READ_NULL_TARGET; }

    struct bmp_header header = {0};
    enum read_status header_status = read_bmp_header(in, &header);
    if (header_status) { return header_status; }
    // create image (allocated)
    struct image_optional optional = image_create(header.biWidth, header.biHeight);
    if (optional.is_valid) {
        *img = optional.image;
    } else {
        return READ_MALLOC_ERROR;
    }
    // set pointer to data and read
    if (fseek(in, header.bOffBits, SEEK_SET)) {
        return READ_STREAM_NULL;
    }
    else {
        read_image_pixels_heap(in, img);
        return READ_OK;
    }
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    if (!out) { return WRITE_ERROR; }
    if (!img) { return WRITE_SOURCE_NULL; }

    struct bmp_header header = {0};
    create_bmp_header(img, &header);
    // write header
    fwrite(&header, sizeof(struct bmp_header), 1, out);
    // set pointer to data and write
    fseek(out, header.bOffBits, SEEK_SET);
    enum write_status write_status = write_image_pixels(out, img);
    if (write_status) { return write_status; }

    return WRITE_OK;
}


static enum read_status read_bmp_header(FILE *in, struct bmp_header *header_ptr) {
    fseek(in, 0, SEEK_END);
    size_t f_size = ftell(in);
    if (f_size < sizeof(struct bmp_header)) { return READ_INVALID_HEADER; }

    rewind(in);
    fread(header_ptr, sizeof(struct bmp_header), 1, in);
    return READ_OK;
}


static void read_image_pixels_heap(FILE *in, struct image *img) {
    int32_t padding = (int32_t) calculate_padding(img->width);
    for (size_t i = 0; i < img->height; ++i) {
        fread(img->pixels + i * (img->width), (size_t) (img->width) * sizeof(struct pixel), 1, in);
        fseek(in, padding, SEEK_CUR);
    }
}

static enum write_status create_bmp_header(struct image const *img, struct bmp_header *header) {
    size_t padding = calculate_padding(img->width);
    header->bfType = SIGNATURE;
    header->biSizeImage = (img->width * sizeof(struct pixel) + padding) * img->height;
    header->bfileSize = header->biSizeImage + sizeof(struct bmp_header);
    header->bfReserved = RESERVED;
    header->bOffBits = sizeof(struct bmp_header);
    header->biSize = HEADER_SIZE;
    header->biWidth = img->width;
    header->biHeight = img->height;
    header->biPlanes = PLANES;
    header->biBitCount = BIT_COUNT;
    header->biCompression = COMPRESSION;
    header->biXPelsPerMeter = PIXEL_PER_M;
    header->biYPelsPerMeter = PIXEL_PER_M;
    header->biClrUsed = COLORS_USED;
    header->biClrImportant = COLORS_IMPORTANT;
    return WRITE_OK;
}

static enum write_status write_image_pixels(FILE* file, const struct image* img) {
    size_t padding = calculate_padding(img->width);
    // malloc padding and set 0
    uint8_t line_padding[8] = {0};
    // write pixels
    if (img->pixels != NULL){
        for (size_t i = 0; i < img->height; ++i) {
            fwrite(img->pixels + i * img->width, img->width * sizeof(struct pixel), 1, file);
            fwrite(line_padding, padding, 1, file);
        }
    }
    return WRITE_OK;
}

static size_t calculate_padding(size_t width) {
    return DOUBLE_WORD - width * (BIT_COUNT / 8) % DOUBLE_WORD;
}
