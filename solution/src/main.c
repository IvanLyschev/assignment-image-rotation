#include <stdio.h>

#include "bmp/bmp.h"
#include "file/file.h"
#include "transform/transformRotate.h"

int main( int argc, char** argv ) {

    if (argc < 3) {
        fprintf(stderr, "Bad arguments\n");
        return 1;
    }
    char *input_fn = argv[1];
    char *output_fn = argv[2];

    FILE *input_file = NULL, *output_file = NULL;
    bool in_bool, out_bool;
    in_bool = open_file(&input_file, input_fn, "rb");
    out_bool = open_file(&output_file, output_fn, "wb");
    if (in_bool|| out_bool) {
        fprintf(stderr, "Cannot open files error\n");
        return 1;
    }
    // reading image
    struct image image = {0};
    if (from_bmp(input_file, &image)){
        fprintf(stderr, "Cannot convert bmp file");
        return 1;
    }
    // rotation
    struct image_optional image_rotated = rotate(image);
    if (!image_rotated.is_valid) {
        fprintf(stderr, "Rotated image creation error");
        return 1;
    }
    // writing
    if (to_bmp(output_file, &image_rotated.image)){
        fprintf(stderr, "Write image to file error");
        return 1;
    }
    // file closing
    in_bool = close_file(&input_file);
    out_bool = close_file(&output_file);
    if (in_bool || out_bool){
        fprintf(stderr, "Files close error\n");
        return 1;
    }
    // destroying images
    image_free(&image_rotated.image);
    image_free(&image);
    return 0;
}

